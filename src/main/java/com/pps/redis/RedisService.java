package com.pps.redis;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pps.redis.object.APIRequest;
import com.pps.redis.object.UserHistory;
import com.pps.redis.object.UserHistoryRepository;

@Component
public class RedisService {
	
	@Autowired
	private UserHistoryRepository userHistoryRepository;
	
	public void storeRequest(String jti, String url) {
		System.out.println("storing [" + jti + "]");
		
		try {
			UserHistory userHistory = this.findUserByUUID(jti);
			
			if (userHistory == null) {
				userHistory = new UserHistory(jti);
				
				System.out.println("does not exist");
			}
			
			APIRequest apiRequest = new APIRequest(UUID.randomUUID().toString(), url, LocalDateTime.now());
			
			userHistory.addAPIRequest(apiRequest);
			
			userHistoryRepository.save(userHistory);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public UserHistory findUserWorkflowHistory(String jti) {
		return this.findUserByUUID(jti);
	}
	
	private UserHistory findUserByUUID(String uuid) {
		Optional<UserHistory> userHistory = userHistoryRepository.findById(uuid);
		
		return userHistory.isPresent() == true ? userHistory.get() : null;
	}
}

