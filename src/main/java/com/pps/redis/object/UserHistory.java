package com.pps.redis.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("user_workflows")
public class UserHistory implements Serializable {
	
	private static final long serialVersionUID = -647782310889716697L;
	
	@Id private String uuid;
	private List<APIRequest> apiRequests;
	
	public UserHistory() {}
	
	public UserHistory(String uuid) {
		this.uuid = uuid;
	}
	
	public UserHistory(String uuid, List<APIRequest> apiRequests) {
		this.uuid = uuid;
		this.apiRequests = apiRequests;
	}
	
	public void addAPIRequest(APIRequest apiRequest) {
		if (this.apiRequests == null) {
			this.apiRequests = new ArrayList<APIRequest>();
		}
		
		this.apiRequests.add(apiRequest);
	}

	public String getUuid() {
		return uuid;
	}

	public List<APIRequest> getApiRequests() {
		return apiRequests;
	}

	@Override
	public String toString() {
		return "UserHiatory [uuid=" + uuid + ", apiRequests=" + apiRequests + "]";
	}
}
