package com.pps;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.connection.RedisClusterConnection;
import org.springframework.data.redis.connection.RedisClusterNode;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.ClusterOperations;
import org.springframework.data.redis.core.RedisTemplate;

import static java.lang.System.exit;

@SpringBootApplication
public class SpringApp implements CommandLineRunner {
	
	@Autowired
	private RedisTemplate<?, ?> redisTemplate;
	
	@Autowired
	private RedisConnectionFactory redisConnectionFactory;
	
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(SpringApp.class);
		
        app.run(args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("running");
		
		RedisClusterConnection redisConnection = null;
		
		try {
			ClusterOperations<?, ?> clusterOps = this.redisTemplate.opsForCluster();
			
			redisConnection = redisConnectionFactory.getClusterConnection();
			
			Iterable<RedisClusterNode> l = redisConnection.clusterGetNodes();
			
			for (RedisClusterNode n : l) {
				System.out.println(n.getId() + " :: " + n.getType());
				
				if (n.isMaster() == true) {
					clusterOps.flushDb(n);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			redisConnection.close();
		}
		
		exit(0);
	}
}
